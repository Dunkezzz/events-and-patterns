const JoinModule = (function() {

    const mainContainer = document.createElement('div');
    mainContainer.classList.add('join-container');
  
    const myImage = document.createElement('img');
  
    const h2 = document.createElement('h2');
    h2.textContent = 'Join Our Program';
  
    const p = document.createElement('p');
    p.classList.add('moreInfo');
    p.textContent = ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae !';
  
    const button = document.createElement('button');
    button.classList.add('btn');
    button.textContent = 'subscribe';
  
    const inputEmail = document.createElement('input');
    inputEmail.placeholder = 'Email';
  
    const subscribeSection = document.createElement('section');
    subscribeSection.classList.add('sub-section');
  
    function showMore() {
      document.querySelector('.moreInfo').style.display = 'block';
    }
  
    return {
      init: function() {
        document.getElementById('join-wrapper').appendChild(mainContainer);
        mainContainer.appendChild(myImage);
        mainContainer.appendChild(h2);
        mainContainer.appendChild(p);
        mainContainer.appendChild(button);
        mainContainer.appendChild(inputEmail);
        mainContainer.appendChild(subscribeSection);
        subscribeSection.appendChild(inputEmail);
        subscribeSection.appendChild(button);
  
        button.addEventListener('click', showMore);
      }
    };
})();

export { JoinModule };
  



