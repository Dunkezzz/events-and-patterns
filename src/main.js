import { JoinModule } from './join-us-section.js';



const SectionCreator = (function() {
  function create(type) {
  switch (type) {
    case 'standard':
  return JoinModule.init();
    case 'advanced':
      let advancedContainer = document.createElement('div')

      advancedContainer.classList.add('join-container')

      const myImage = document.createElement('img')

      const h2 = document.createElement('h2')
      h2.textContent = 'Join Our Advanced Program'

      const p = document.createElement('p')
      p.classList.add('moreInfo')

      p.textContent= ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae !'


      const button = document.createElement('button')
      
      button.setAttribute("id", "button-wide")
      button.textContent= 'subscribe to advanced program'


      const inputEmail = document.createElement('input')
      inputEmail.placeholder = 'Email'



      const subscribeSection = document.createElement ('section')
      subscribeSection.classList.add ('sub-section')



      document.getElementById('join-wrapper').appendChild(advancedContainer)
      advancedContainer.appendChild(myImage)
      advancedContainer.appendChild(h2)
      advancedContainer.appendChild(p)
      advancedContainer.appendChild(button)
      advancedContainer.appendChild(inputEmail)
      advancedContainer.appendChild(subscribeSection)
      subscribeSection.appendChild(inputEmail)
      subscribeSection.appendChild(button)


      function showMore(){
        document.querySelector('.moreInfo').style.display ='block'
      }

      document.querySelector('.btn').addEventListener('click', showMore)   

        return mainContainer;
      default:
        throw new Error(`Invalid type: ${type}`); // если вообще другой тип то возвращаем ошибку
    }
  }

  return {
    create
  };
})();



// const standardProgram = SectionCreator.create('standard');
// document.getElementById('join-wrapper').appendChild(standardProgram);

const advancedProgram = SectionCreator.create('advanced');
document.getElementById('advanced-wrapper').appendChild(advancedProgram);


setTimeout(() => {
  const subscribeButton = document.querySelector('.btn');
  if (subscribeButton) {
    subscribeButton.addEventListener('click', showMore);
  }
}, 100);


